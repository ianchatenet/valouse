# Valouse

## Requirements

node 21.5.0

## Getting started

Install project's dependencies `npm install`

Add a `.env` file with 

```
NODE_ENV=production
PORT=yourport
HOST=yourhost
VITE_PORT=yourport
VITE_HOST=yourHost
```

Launch the server `npm start`

Launch with docker compose `docker compose up`