import { Params } from 'fastify-cron';

import { CleanRoomsCron } from './cleanRooms/cleanRooms.cron';
import { RoomRepository } from '../repositories/rooms/roomRepository';

export function getCronJobs(roomRepository: RoomRepository): Params[] {
  const cleanRoomsCron = new CleanRoomsCron(roomRepository);

  const cleanRoomsJob: Params = {
    cronTime: '* * * * *',
    onTick: cleanRoomsCron.run.bind(cleanRoomsCron),
    startWhenReady: true
  };

  return [cleanRoomsJob,];
}