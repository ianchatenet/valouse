import { FastifyInstance } from 'fastify';

import { Room } from '../../models/room.model';
import { RemoveRoomDto } from '../../dtos/rooms/removeRoom.dto';
import { RoomRepositoryI } from '../../repositories/rooms/roomRepository.interface';

export class CleanRoomsCron {
  name: string = this.constructor.name;

  private roomRepository: RoomRepositoryI;

  constructor(roomRepository: RoomRepositoryI, name?: string) {
    this.roomRepository = roomRepository;
    if (name) this.name = name;
  }

  async run(server: FastifyInstance): Promise<void> {
    server.log.info(`CRON JOB: starting ${this.name}`);
    const rooms: Room[] = await this.roomRepository.findAll();

    const dto: RemoveRoomDto = [];
    for (let i = 0; i < rooms.length; i++) {
      const room = rooms[i];
      const oneHour = 60 * 60 * 1000;

      const roomDateTime = new Date(room.created_at).getTime();
      const now = new Date().getTime();
      const connectedUsers: number = room.connectedUsers.length;

      if (now - roomDateTime > oneHour && connectedUsers === 0) {
        dto.push(room.id);
      }
    }

    await this.roomRepository.remove(dto);
    server.log.info(`CRON JOB: ${this.name} done`);
  }
}