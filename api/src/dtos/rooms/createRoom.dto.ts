import { Room } from '../../models/room.model';

type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export type CreateRoomDto = Optional<Omit<Room, 'id' | 'connectedUsers' | 'created_at' | 'updated_at'>, 'password'>;