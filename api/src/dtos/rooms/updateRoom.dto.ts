import { Room } from '../../models/room.model';
import { CreateRoomDto } from './createRoom.dto';

export type UpdateRoomDto = Partial<CreateRoomDto> & {
  id: number
} & Partial<Pick<Room, 'connectedUsers'>>;