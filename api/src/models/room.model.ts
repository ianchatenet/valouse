export type Room = {
  id: number;
  name: string;
  size: number;
  connectedUsers: string[];
  private: boolean;
  password?: string;
  created_at: Date;
  updated_at: Date;
}