import { Room } from '../../models/room.model';
import { RoomRepositoryI } from './roomRepository.interface';
import { JsonDbI } from '../../adapters/db/json.db.interface';
import { CreateRoomDto } from '../../dtos/rooms/createRoom.dto';
import { UpdateRoomDto } from '../../dtos/rooms/updateRoom.dto';
import { RemoveRoomDto } from '../../dtos/rooms/removeRoom.dto';
import { NotFoundError, NotFoundErrors } from '../../core/errors/notFoundError';

export class RoomRepository implements RoomRepositoryI {
  jsonDb: JsonDbI;

  constructor(jsonDb: JsonDbI) {
    this.jsonDb = jsonDb;
  }

  async findAll(): Promise<Room[]> {
    const db = await this.jsonDb.read();
    const rooms: Room[] = db.rooms;
    return rooms;
  }

  async find(id: number): Promise<Room> {
    const db = await this.jsonDb.read();
    const rooms: Room[] = db.rooms;
    const room: Room | undefined = rooms.find(elmt => elmt.id === id);
    if (!room) throw new NotFoundError(NotFoundErrors.RoomNotFound);
    return room;
  }

  async create(dto: CreateRoomDto): Promise<Room> {
    const db = await this.jsonDb.read();
    const rooms: Room[] = db.rooms ?? [];

    const newRoom: Room = {
      id: rooms.length + 1,
      name: dto.name,
      size: dto.size,
      connectedUsers: [],
      private: dto.private,
      password: dto.password,
      created_at: new Date(),
      updated_at: new Date()
    };

    rooms.push(newRoom);
    db.rooms = rooms;

    await this.jsonDb.write(db);
    return newRoom;
  }

  async update(dto: UpdateRoomDto): Promise<Room> {
    const db = await this.jsonDb.read();
    const rooms: Room[] = db.rooms;
    const roomIndex: number | undefined = rooms.findIndex(elmt => elmt.id === dto.id);
    if (roomIndex === undefined) throw new NotFoundError(NotFoundErrors.RoomNotFound);

    const room: Room = rooms[roomIndex];

    dto.name ? room.name = dto.name : null;
    dto.size ? room.size = dto.size : null;
    dto.connectedUsers ? room.connectedUsers = dto.connectedUsers : null;
    dto.private ? room.private = dto.private : null;
    dto.password ? room.password = dto.password : null;

    room.updated_at = new Date();

    rooms[roomIndex] = room;
    db.rooms = rooms;

    await this.jsonDb.write(db);
    return room;
  }

  async remove(dto: RemoveRoomDto): Promise<void> {
    const db = await this.jsonDb.read();
    const rooms: Room[] = db.rooms;

    if (dto instanceof Array) {
      const newRooms = rooms.filter((room) => dto.includes(room.id) === false);
      db.rooms = newRooms;
    } else {
      const roomIndex: number | undefined = rooms.findIndex(elmt => elmt.id === dto);
      if (roomIndex === undefined) throw new NotFoundError(NotFoundErrors.RoomNotFound);
      rooms.splice(roomIndex, 1);
    }

    await this.jsonDb.write(db);
    return;
  }
}