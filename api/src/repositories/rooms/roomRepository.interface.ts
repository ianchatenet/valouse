import { Room } from '../../models/room.model';
import { CreateRoomDto } from '../../dtos/rooms/createRoom.dto';
import { UpdateRoomDto } from '../../dtos/rooms/updateRoom.dto';
import { RemoveRoomDto } from '../../dtos/rooms/removeRoom.dto';

export interface RoomRepositoryI {
  findAll(): Promise<Room[]>;
  find(id: number): Promise<Room>;
  create(dto: CreateRoomDto): Promise<Room>;
  update(dto: UpdateRoomDto): Promise<Room>;
  remove(dto: RemoveRoomDto): Promise<void>;
}