import { FastifyRequest } from 'fastify';

import { Params } from 'fastify-cron';
import { SocketStream } from '@fastify/websocket';

import { RouteI } from '../routes/route.interface';

export type ServerOptions = {
  plugins?: any[];
  api?: {
    routes: RouteI[];
    path: string;
  }
  cronJobs?: Params[];
  port: number;
  host: string;
  wsHandler: WsHandlerI;
}

export interface WsHandlerI {
  run(connection: SocketStream, req: FastifyRequest): void;
}