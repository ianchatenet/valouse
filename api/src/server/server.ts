import { cwd } from 'process';
import cors from '@fastify/cors';
import { join } from 'node:path';
import ws from '@fastify/websocket';
import servestatic from '@fastify/static';
import fastifyCron, { Params } from 'fastify-cron';
import fastify, { FastifyInstance } from 'fastify';

import { RouteI } from '../routes/route.interface';
import { ServerOptions, WsHandlerI } from './serverOptions';
import { errorHandler } from '../core/errorHandler/errorHandler';

export class Server {
  private readonly fastify: FastifyInstance;

  private readonly port: number;
  private readonly host: string;
  private readonly wsHandler: WsHandlerI;

  private readonly apiPath?: string;
  private readonly cronJobs?: Params[];

  constructor(opts: ServerOptions) {
    this.port = opts.port;
    this.host = opts.host;

    this.fastify = fastify({ logger: true });

    this.fastify.register(cors, {
      origin: [
        new RegExp(`^http://localhost:${this.port}`),
        new RegExp(`^http://${this.host}:${this.port}`)
      ]
    });
    
    this.fastify.register(ws);


    if (opts.api) {
      this.apiPath = opts.api.path;
      this.addRoutes(opts.api.routes);
    }

    this.cronJobs = opts.cronJobs;

    this.wsHandler = opts.wsHandler;

    this.handleWs();

    this.fastify.register(servestatic, {
      root: join(cwd(), './public')
    });

    this.fastify.setErrorHandler(errorHandler);

    if (this.cronJobs) {
      this.fastify.register(fastifyCron, {
        jobs: this.cronJobs
      });
    }
  }

  private addRoutes(routes: RouteI[]) {

    this.fastify.get('/', function (req, reply) {
      reply.sendFile('index.html');
    });

    this.fastify.setNotFoundHandler((req, res) => {
      res.sendFile('index.html');
    });

    for (let i = 0; i < routes.length; i++) {
      const route: RouteI = routes[i];
      const routeUrl = this.apiPath + route.path;

      if (route.find) {
        this.fastify.get(routeUrl + '/:id', route.find.bind(route));
      }

      if (route.findAll) {
        this.fastify.get(routeUrl, route.findAll.bind(route));
      }

      if (route.create) {
        this.fastify.post(routeUrl, route.create.bind(route));
      }

      if (route.update) {
        this.fastify.patch(routeUrl, route.update.bind(route));
      }

      if (route.remove) {
        this.fastify.delete(routeUrl, route.remove.bind(route));
      }
    }
  }

  async listen() {
    try {
      await this.fastify.listen({ port: this.port, host: this.host });
    } catch (err) {
      this.fastify.log.error(err);
      process.exit(1);
    }
  }

  private handleWs() {

    this.fastify.register(async (fastify) => {
      fastify.get('/chat/:id', { websocket: true }, this.wsHandler.run.bind(this.wsHandler));
    });
  }
}