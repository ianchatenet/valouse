import { FastifyReply } from 'fastify';

import { FastifyRequest } from 'fastify/types/request';

import { RouteI } from '../route.interface';
import { Room } from '../../models/room.model';
import { CreateRoomDto } from '../../dtos/rooms/createRoom.dto';
import { UpdateRoomDto } from '../../dtos/rooms/updateRoom.dto';
import { FindRoomUsecaseI } from '../../usecases/rooms/find/findRoom.interface';
import { CreateRoomUsecaseI } from '../../usecases/rooms/create/createRoom.interface';
import { UpdateRoomUsecaseI } from '../../usecases/rooms/update/updateRoom.interface';
import { RemoveRoomUsecaseI } from '../../usecases/rooms/remove/removeRoom.interface';
import { FindAllRoomUsecaseI } from '../../usecases/rooms/findAll/findAllRoom.interface';

export class RoomsRoute implements RouteI {
  path = '/rooms';

  private getAllRoom: FindAllRoomUsecaseI;
  private getRoom: FindRoomUsecaseI;
  private createRoom: CreateRoomUsecaseI;
  private updateRoom: UpdateRoomUsecaseI;
  private removeRoom: RemoveRoomUsecaseI;

  constructor(
    getAllRoom: FindAllRoomUsecaseI,
    getRoom: FindRoomUsecaseI,
    createRoom: CreateRoomUsecaseI,
    updateRoom: UpdateRoomUsecaseI,
    removeRoom: RemoveRoomUsecaseI,
  ) {
    this.getAllRoom = getAllRoom;
    this.getRoom = getRoom;
    this.createRoom = createRoom;
    this.updateRoom = updateRoom;
    this.removeRoom = removeRoom;
  }

  async find(req: FastifyRequest): Promise<Room[] | Room> {
    const params: any = req.params as any;
    const id = parseInt(params.id);
    const room: Room = await this.getRoom.run(id);
    return room;
  }

  async findAll(): Promise<Room[] | Room> {
    const rooms: Room[] = await this.getAllRoom.run();
    return rooms;
  }

  async create(req: FastifyRequest, res: FastifyReply) {
    const body = req.body;
    res.status(201);
    return this.createRoom.run(body as CreateRoomDto);
  }

  async update(req: FastifyRequest): Promise<Room> {
    const body = req.body;
    return this.updateRoom.run(body as UpdateRoomDto);
  }

  async remove(req: FastifyRequest, res: FastifyReply) {
    const body = req.body as { id: string };
    const id = parseInt(body.id);
    await this.removeRoom.run(id);
    res.status(204);
  }
} 