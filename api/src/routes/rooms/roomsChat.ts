import { FastifyRequest } from 'fastify';

import { SocketStream } from '@fastify/websocket';

import { Room } from '../../models/room.model';
import { WsHandlerI } from '../../server/serverOptions';
import { UpdateRoomDto } from '../../dtos/rooms/updateRoom.dto';
import { FindRoomUsecaseI } from '../../usecases/rooms/find/findRoom.interface';
import { UpdateRoomUsecaseI } from '../../usecases/rooms/update/updateRoom.interface';

type QueryParams = {
  username: string;
  password?: string;
}

export type RoomMessage = {
  from: string,
  message: string,
  date: Date,
  roomInfos: {
    users: string[]
  }
}

type Client = {
  roomId: number,
  clientUsername: string,
  joiningDate: Date,
  connection: SocketStream
}

export class RoomsChat implements WsHandlerI {

  private getRoom: FindRoomUsecaseI;
  private updateRoom: UpdateRoomUsecaseI;
  private clients: Client[] = [];

  constructor(getRoom: FindRoomUsecaseI, updateRoom: UpdateRoomUsecaseI) {
    this.getRoom = getRoom;
    this.updateRoom = updateRoom;
  }

  async run(connection: SocketStream, req: FastifyRequest) {
    connection.setEncoding('utf8');
    const params: any = req.params as any;
    const id = parseInt(params.id);

    const queryParams = req.query as QueryParams;

    if (!queryParams.username) {
      throw new Error('NEED_USERNAME');
    }

    await this.onConnect(connection, queryParams, id);
    this.onMessage(connection, queryParams, id);
    await this.onClose(connection, queryParams, id);
  }

  send(message: RoomMessage, roomId: number) {
    for (let i = 0; i < this.clients.length; i++) {
      const client = this.clients[i];
      if (client.roomId !== roomId) continue;

      client.connection.socket.send(JSON.stringify(message));
    }
  }

  private async onConnect(connection: SocketStream, queryParams: QueryParams, roomId: number) {

    const room: Room = await this.getRoom.run(roomId);
    const connectedUsers: string[] = room.connectedUsers;

    connectedUsers.push(queryParams.username);
    const updateDto: UpdateRoomDto = {
      id: room.id,
      connectedUsers: connectedUsers,
    };

    await this.updateRoom.run(updateDto);

    const res: RoomMessage = {
      from: 'server',
      message: `${queryParams.username} joined the room.`,
      date: new Date(),
      roomInfos: {
        users: connectedUsers
      }
    };

    const client: Client = {
      clientUsername: queryParams.username,
      roomId: room.id,
      joiningDate: new Date(),
      connection
    };

    this.clients.push(client);

    this.send(res, room.id);
  }

  private onMessage(connection: SocketStream, queryParams: QueryParams, roomId: number) {
    connection.socket.on('message', async (message) => {
    const room: Room = await this.getRoom.run(roomId);

      const res: RoomMessage = {
        from: queryParams.username,
        message: message.toString(),
        date: new Date(),
        roomInfos: {
          users: room.connectedUsers
        }
      };

      this.send(res, room.id);
    });
  }

  private async onClose(connection: SocketStream, queryParams: QueryParams, roomId: number) {
    connection.socket.on('close', async () => {
      const room: Room = await this.getRoom.run(roomId);
      const connectedUsers = room.connectedUsers.filter(elt => elt !== queryParams.username);

      const updateDto: UpdateRoomDto = {
        id: room.id,
        connectedUsers: connectedUsers
      };

      await this.updateRoom.run(updateDto);

      const res: RoomMessage = {
        from: 'server',
        message: `${queryParams.username} leaved the room.`,
        date: new Date(),
        roomInfos: {
          users: connectedUsers
        }
      };

      this.send(res, room.id);
    });
  }
}