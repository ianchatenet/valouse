import { FastifyReply, FastifyRequest } from 'fastify';

export interface RouteI {
  path: string;
  find?: (req: FastifyRequest, res: FastifyReply) => void;
  findAll?: (req: FastifyRequest, res: FastifyReply) => void;
  create?: (req: FastifyRequest, res: FastifyReply) => void;
  update?: (req: FastifyRequest, res: FastifyReply) => void;
  remove?: (req: FastifyRequest, res: FastifyReply) => void;
}