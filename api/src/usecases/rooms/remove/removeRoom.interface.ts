export interface RemoveRoomUsecaseI {
  run(id: number): Promise<void>;
}