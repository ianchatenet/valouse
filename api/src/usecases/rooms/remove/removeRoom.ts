import { RemoveRoomUsecaseI } from './removeRoom.interface';
import { RoomRepositoryI } from '../../../repositories/rooms/roomRepository.interface';

export class RemoveRoomUsecase implements RemoveRoomUsecaseI {
  private roomRepository: RoomRepositoryI;

  constructor(roomRepository: RoomRepositoryI) {
    this.roomRepository = roomRepository;
  }

  run(id: number): Promise<void> {
    return this.roomRepository.remove(id);
  }
}