import { CreateRoomUsecaseI } from './createRoom.interface';
import { CreateRoomDto } from '../../../dtos/rooms/createRoom.dto';
import { RoomRepositoryI } from '../../../repositories/rooms/roomRepository.interface';
import { Room } from '../../../models/room.model';

export class CreateRoomUsecase implements CreateRoomUsecaseI {
  private roomRepository: RoomRepositoryI;

  constructor(roomRepository: RoomRepositoryI) {
    this.roomRepository = roomRepository;
  }

  async run(dto: CreateRoomDto): Promise<Room> {

    if (!dto.private) dto.private = false;
    if (!dto.size) dto.size = 0;

    if (!dto.name) throw new Error('ROOM_NEED_NAME');
    if (dto.private === true && !dto.password) throw new Error('ROOM_PRIVATE_NEED_PASSWORD');

    return this.roomRepository.create(dto);
  }
}