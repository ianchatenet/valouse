import { CreateRoomDto } from '../../../dtos/rooms/createRoom.dto';
import { Room } from '../../../models/room.model';

export interface CreateRoomUsecaseI {
  run(dto: CreateRoomDto): Promise<Room>;
}