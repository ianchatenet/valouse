import { Room } from '../../../models/room.model';

export interface FindAllRoomUsecaseI {
  run(): Promise<Room[]>;
}