import { Room } from '../../../models/room.model';
import { FindAllRoomUsecaseI } from './findAllRoom.interface';
import { RoomRepositoryI } from '../../../repositories/rooms/roomRepository.interface';

export class FindAllRoomUsecase implements FindAllRoomUsecaseI {
  private roomRepository: RoomRepositoryI;

  constructor(roomRepository: RoomRepositoryI) {
    this.roomRepository = roomRepository;
  }

  async run(): Promise<Room[]> {
    const rooms: Room[] = await this.roomRepository.findAll();
    return rooms;
  }
}