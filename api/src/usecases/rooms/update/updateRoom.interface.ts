import { Room } from '../../../models/room.model';
import { UpdateRoomDto } from '../../../dtos/rooms/updateRoom.dto';

export interface UpdateRoomUsecaseI {
  run(dto: UpdateRoomDto): Promise<Room>;
}