import { Room } from '../../../models/room.model';
import { UpdateRoomUsecaseI } from './updateRoom.interface';
import { UpdateRoomDto } from '../../../dtos/rooms/updateRoom.dto';
import { RoomRepositoryI } from '../../../repositories/rooms/roomRepository.interface';

export class UpdateRoomUsecase implements UpdateRoomUsecaseI {
  private roomRepository: RoomRepositoryI;

  constructor(roomRepository: RoomRepositoryI) {
    this.roomRepository = roomRepository;
  }

  run(dto: UpdateRoomDto): Promise<Room> {
    return this.roomRepository.update(dto);
  }
}