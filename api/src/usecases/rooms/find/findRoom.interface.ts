import { Room } from '../../../models/room.model';

export interface FindRoomUsecaseI {
  run(id: number): Promise<Room>;
}