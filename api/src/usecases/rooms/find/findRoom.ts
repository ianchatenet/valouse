import { Room } from '../../../models/room.model';
import { FindRoomUsecaseI } from './findRoom.interface';
import { RoomRepositoryI } from '../../../repositories/rooms/roomRepository.interface';

export class FindRoomUsecase implements FindRoomUsecaseI {
  private roomRepository: RoomRepositoryI;

  constructor(roomRepository: RoomRepositoryI) {
    this.roomRepository = roomRepository;
  }

  async run(id: number): Promise<Room> {
    const room = await this.roomRepository.find(id);
    return room;
  }
}