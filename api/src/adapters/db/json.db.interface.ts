import { Room } from '../../models/room.model';

export interface JsonDbI {
  read: () => Promise<{ rooms: Room[] }>;
  write: (content: { rooms: Room[] }) => Promise<true>;
}