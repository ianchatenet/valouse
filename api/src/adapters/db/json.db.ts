import { readFile, writeFile, access } from 'fs/promises';

import { JsonDbI } from './json.db.interface';
import { Room } from '../../models/room.model';

export class JsonDb implements JsonDbI {

  private readonly dbJsonPath = 'db.json';
  private readonly baseDbSchema = {
    rooms: []
  };

  async read(): Promise<{ rooms: Room[] }> {
    try {
      await access(this.dbJsonPath);
    } catch (error: any) {
      if (error.code && error.code === 'ENOENT') {
        await writeFile(this.dbJsonPath, JSON.stringify(this.baseDbSchema));
      } else {
        throw error;
      }
    }

    const rawDb = await readFile(this.dbJsonPath, 'utf-8');
    const db = JSON.parse(rawDb);
    return db;
  }

  async write(content: { rooms: Room[] }): Promise<true> {
    const stringContent = JSON.stringify(content);
    await writeFile(this.dbJsonPath, stringContent);
    return true;
  }
}