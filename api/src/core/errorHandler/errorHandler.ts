import { FastifyReply, FastifyRequest } from 'fastify';

import { ApiError } from '../errors/apiError';

export function errorHandler(error: ApiError, req: FastifyRequest, rep: FastifyReply) {
  rep.status(error.statusCode);
  rep.send(error);
}