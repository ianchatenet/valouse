import { ApiError } from './apiError';

export class NotFoundError extends ApiError {

  constructor(msg?: NotFoundErrors){
    super(msg ?? 'NOT_FOUND', 404);
  }
}

export enum NotFoundErrors {
  RoomNotFound = 'ROOM_NOT_FOUND'
}