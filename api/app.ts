import { Params } from 'fastify-cron';

import { Server } from './src/server/server';
import { getCronJobs } from './src/cronJobs';
import { JsonDb } from './src/adapters/db/json.db';
import { RouteI } from './src/routes/route.interface';
import { RoomsChat } from './src/routes/rooms/roomsChat';
import { RoomsRoute } from './src/routes/rooms/roomsRoute';
import { ServerOptions } from './src/server/serverOptions';
import { FindRoomUsecase } from './src/usecases/rooms/find/findRoom';
import { RoomRepository } from './src/repositories/rooms/roomRepository';
import { CreateRoomUsecase } from './src/usecases/rooms/create/createRoom';
import { UpdateRoomUsecase } from './src/usecases/rooms/update/updateRoom';
import { RemoveRoomUsecase } from './src/usecases/rooms/remove/removeRoom';
import { FindAllRoomUsecase } from './src/usecases/rooms/findAll/findAllRoom';

const jsonDb = new JsonDb();

// repositories
const roomRepository = new RoomRepository(jsonDb);

const findAllRoom = new FindAllRoomUsecase(roomRepository);
const findRoom = new FindRoomUsecase(roomRepository);
const createRoom = new CreateRoomUsecase(roomRepository);
const updateRoom = new UpdateRoomUsecase(roomRepository);
const removeRoom = new RemoveRoomUsecase(roomRepository);

// routes
const roomsRoute = new RoomsRoute(findAllRoom, findRoom, createRoom, updateRoom, removeRoom);

const routes: RouteI[] = [roomsRoute];

const wsHandler = new RoomsChat(findRoom, updateRoom);
// init server

const jobs: Params[] = getCronJobs(roomRepository);

const port = process.env.PORT ? parseInt(process.env.PORT) : 80;
const host = process.env.HOST ? process.env.HOST : 'localhost';
const opts: ServerOptions = { api: { path: '/api', routes }, port: port, host: host, wsHandler, cronJobs: jobs };
const server = new Server(opts);

server.listen();