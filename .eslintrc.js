module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:solid/recommended'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'solid'],
  env: {
    node: true
  },
  rules: {
    'no-var': 'error',
    'no-return-await': 'error',
    'prefer-const': 'error',
    'array-bracket-newline': ['error', { 'multiline': true, 'minItems': 2 }],
    'array-element-newline': ['error', { 'multiline': true, 'minItems': 2 }],
    'array-bracket-spacing': 'error',
    eqeqeq: 'error',
    'no-console': 'error',
    'max-nested-callbacks': ['error', 3],
    quotes: ['error', 'single', {
      'avoidEscape': true
    }],
    'semi': ['error', 'always'],
    'no-fallthrough': 'error',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        'selector': 'variable',
        'format': ['camelCase']
      },
      {
        'selector': 'variable',
        'modifiers': ['const'],
        'format': ['camelCase', 'UPPER_CASE']
      },
      {
        'selector': 'typeLike',
        'format': ['PascalCase']
      },
      {
        'selector': 'class',
        'format': ['PascalCase']
      },
      {
        'selector': 'interface',
        'format': ['PascalCase'],
        'custom': {
          'regex': '^I[A-Z]',
          'match': false
        }
      },
    ],
    'max-len': [
      'warn',
      {
        'code': 100,
        'tabWidth': 2,
        'comments': 100,
        'ignoreComments': false,
        'ignoreTrailingComments': true,
        'ignoreUrls': true,
        'ignoreStrings': true,
        'ignoreTemplateLiterals': true,
        'ignoreRegExpLiterals': true
      }
    ]
  }
};
