/* @refresh granular */

import { Routes } from './routes';
import { Router } from '@solidjs/router';
import { ErrorBoundary } from 'solid-js';

import Error from './pages/error/Error';
import Header from './components/header/Header';

import './app.scss';

export default function App() {

  return (
    <>
      <head>
        <title>Valouse</title>
        <link rel="icon" type="image/x-icon" href="/assets/images/favicon.png" />
      </head>
      <Router>
        <ErrorBoundary fallback={(err) => <Error error={err} />}>
          <Header />
          <Routes />
        </ErrorBoundary>
      </Router>
    </>
  );
}