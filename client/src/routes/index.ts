import { lazy } from 'solid-js';
import { RouteDefinition, useRoutes } from '@solidjs/router';

export const rootPath = '/';
export const homePath = '/home';
export const roomsPath = (id: string) => `/rooms/${id}`;
export const anyPath = '/*';

const routes: RouteDefinition[] = [
  {
    path: [rootPath, homePath]
    ,
    component: lazy(() => import('../pages/Home'))
  },
  {
    path: '*',
    component: lazy(() => import('../components/errors/notFound/NotFound'))
  },
  {
    path: '/rooms/:id',
    component: lazy(() => import('../pages/rooms/Room'))
  }
];


// eslint-disable-next-line @typescript-eslint/naming-convention
export const Routes = useRoutes(routes);