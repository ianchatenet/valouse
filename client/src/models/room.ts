export type RoomModel = {
  id: number;
  name: string;
  private: boolean;
  size: number;
  created_at: string;
  updated_at: string;
}