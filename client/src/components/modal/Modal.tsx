/* @refresh granular */

import { ParentProps } from 'solid-js';

type Props = ParentProps & {
  ref: HTMLDialogElement;
}

export default function Modal(props: Props) {
  return (
      <dialog ref={props.ref} class='modal'>
        {props.children}
      </dialog>
  );
}