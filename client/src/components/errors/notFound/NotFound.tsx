/* @refresh granular */

import { A } from '@solidjs/router';
import { rootPath } from '../../../routes';
import { i18n } from '../../../services/i18n/i18n';

import './notFound.css';

export default function NotFound() {

  return (
    <div class='NotFound'>
      <h1>{i18n('404.title')}</h1>
      <p>{i18n('404.text')}</p>
      <A href={rootPath}>{i18n('404.linkHome')}</A>
    </div>
  );
}