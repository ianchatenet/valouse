import { Setter, Show, createSignal } from 'solid-js';
import { CreateRoomDto, createRoom } from '../../services/rooms/createRoom';
import { RoomModel } from '../../models/room';
import { getRooms } from '../../services/rooms/getRooms';

export function RoomCreateForm(props: { setRooms: Setter<RoomModel[]> }) {
  const [isPrivate, setIsPrivate] = createSignal<boolean>(false);

  return (
    <>
      <form method='dialog' onSubmit={(e) => handleOnSubmit(e, props.setRooms)}>
        <fieldset>
          <label>Room's name</label>
          <input name='roomName' required />

          <label>Room's size</label>
          <input name='roomSize' type='number' min='0' />

          <legend>Is the room private</legend>
          <label>
            <input name='roomPrivacy' type='checkbox' onChange={(e) => setIsPrivate(e.target.checked)} />
            Is the room private
          </label>

          <Show when={isPrivate() === true} fallback={<></>}>
            <label>Room's password</label>
            <input name='roomPassword' type='text' required />
          </Show>

          <button type='submit'>Submit</button>
        </fieldset>
      </form>
    </>
  );
}

async function handleOnSubmit(e: Event & {
  submitter: HTMLElement;
} & {
  currentTarget: HTMLFormElement;
  target: Element;
}, setRooms: Setter<RoomModel[]>) {
  e.preventDefault();
  const form = e.currentTarget;

  const roomName = form['roomName'].value;
  const roomSize = form['roomSize'].number ?? 0;
  const roomPrivacy = form['roomPrivacy'].checked;
  const roomPassword = form['roomPassword']?.value;

  const dto: CreateRoomDto = {
    name: roomName,
    size: roomSize,
    private: roomPrivacy,
    password: roomPassword
  };

  try {
    await createRoom(dto);
    form.reset();
    form.submit();
    setRooms(await getRooms());
  } catch (error) {
    alert(error);
  }
}
