export function RoomChat(props: { socket: WebSocket }) {
  const socket:WebSocket = props.socket;

  return (
      <form method='dialog' onSubmit={(e) => handleOnSubmit(e)}>
        <fieldset role='group'>
          <input name='chat' placeholder='type here' autocomplete='off' />
          <button type='submit'>Send</button>
        </fieldset>
      </form>
  );
  
  async function handleOnSubmit(e: Event & {
    submitter: HTMLElement;
  } & {
    currentTarget: HTMLFormElement;
  }){
    e.preventDefault();
    const chatInput: HTMLInputElement = e.currentTarget['chat'];
    const message = chatInput.value;
    socket.send(message);
    chatInput.value = '';
  }
}