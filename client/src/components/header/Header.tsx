/* @refresh granular */

import { A } from '@solidjs/router';

export default function Header() {

  return (
    <header>
      <h1>Valouz chat app</h1>
      <nav>
        <ul>
          <li>
            <A href="/">Home</A>
          </li>
        </ul>
      </nav>
    </header>
  );
}