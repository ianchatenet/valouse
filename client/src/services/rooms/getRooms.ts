import { RoomModel } from '../../models/room';
import { fetchApi } from '../fetchApi/fetchApi';

export async function getRooms(): Promise<RoomModel[]> {
  return fetchApi<RoomModel[]>('rooms');
}