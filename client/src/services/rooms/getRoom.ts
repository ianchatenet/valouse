import { RoomModel } from '../../models/room';
import { fetchApi } from '../fetchApi/fetchApi';

export async function getRoom(id: string): Promise<RoomModel> {
  return fetchApi<RoomModel>(`rooms/${id}`);
}