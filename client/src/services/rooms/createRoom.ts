import { RoomModel } from '../../models/room';
import { fetchApi } from '../fetchApi/fetchApi';

export type CreateRoomDto = {
  name: string;
  size: number;
  private: boolean;
  password?: string;
}

export async function createRoom(dto: CreateRoomDto): Promise<RoomModel> {
  return fetchApi<RoomModel>('rooms', {
    method: 'POST',
    body: JSON.stringify(dto),
    headers: {
      'content-type': 'application/json'
    }
  });
}