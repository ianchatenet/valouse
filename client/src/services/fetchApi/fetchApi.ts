import { ApiError } from '../../../../api/src/core/errors/apiError';

type ApiResponse<T> = ApiError | T;

export async function fetchApi<T extends object>(
  url: string,
  options?: RequestInit
): Promise<T | never> {
  try {
    const apiPort = import.meta.env.VITE_PORT ?? 80;
    const apiHost = import.meta.env.VITE_HOST ?? 'localhost';
    const baseUrl = `http://${apiHost}:${apiPort}/api`;
    const reqUrl = `${baseUrl}/${url}`;

    const res = await fetch(reqUrl, options);
    const data = await res.json() as ApiResponse<T>;

    if (isApiError(data)) {
      throw new ApiError(data.message, data.statusCode);
    }

    return data as T;
  } catch (error) {
    if (error instanceof Error) {
      throw error;
    }

    if (typeof error === 'string') {
      throw new Error(error);
    }

    throw new Error('Fetch error');
  }
}

function isApiError<T>(data: ApiResponse<T>): data is ApiError {
  return (data as ApiError).statusCode !== undefined;
}