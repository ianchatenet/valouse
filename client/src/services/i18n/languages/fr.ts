export const fr = {
  404: {
    title: 'Erreur 404',
    text: 'Page introuvable.',
    linkHome: 'Accueil'
  }
};