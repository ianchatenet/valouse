export const en = {
  404: {
    title: 'Error 404',
    text: 'Page not found.',
    linkHome: 'Home'
  }
};