import { en } from './languages/en';
import { fr } from './languages/fr';

export function i18n(translationKey: string): string {
  const keys = translationKey.split('.');
  const lang = navigator.language;

  let tradObject: I18nKeys = translations[lang];
  if(!tradObject) tradObject = translations.default;

  let tradString = '';
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    if (i === keys.length - 1) {
      tradString = tradObject[key] as string;
    } else {
      tradObject = tradObject[key] as I18nKeys;
    }
  }

  if(!tradString && tradString !== ''){
    throw new Error(`Translation "${translationKey}" not found`);
  }

  if (typeof tradString !== 'string') {
    throw new Error(`Translation "${translationKey}" is not a string`);
  }

  return tradString;
}

type I18nKeys = { [key: string]: I18nKeys | string };
type Translations = Record<string, I18nKeys> & { default: I18nKeys};

const translations: Translations = {
  default: en,
  en,
  fr,
};