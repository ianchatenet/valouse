/* @refresh granular */

import NotFound from '../../components/errors/notFound/NotFound';
import { ApiError } from '../../../../api/src/core/errors/apiError';

export default function Error(props:{error: Error}) {
const error = props.error;

  if(error instanceof ApiError) {
    if(error.statusCode === 404) {
      return <NotFound/>;
    }

    return (
      <div>
        <p>{error.message} code {error.statusCode}</p>
      </div>
    );
  }

  return (
    <div>
      <p>Error</p>
    </div>
  );
}