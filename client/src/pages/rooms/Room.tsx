/* @refresh granular */
import { A, useParams } from '@solidjs/router';
import { createResource, createSignal, For, onCleanup, onMount, Show } from 'solid-js';

import { rootPath } from '../../routes';
import { i18n } from '../../services/i18n/i18n';
import { getRoom } from '../../services/rooms/getRoom';
import { RoomChat } from '../../components/rooms/RoomChat';
import { LocalStorageKeys } from '../../enums/localStorageKeys.enum';
import { RoomMessage } from '../../../../api/src/routes/rooms/roomsChat';

import './room.css';

export default function Room() {
  const { id } = useParams<{ id: string }>();

  const username = localStorage.getItem(LocalStorageKeys.Username);

  if (!username) {
    return (
      <main>
        You need a name to join a room
        <A href={rootPath}>{i18n('404.linkHome')}</A>
      </main>
    );
  }

  const apiPort = import.meta.env.VITE_PORT ?? 80;
  const apiHost = import.meta.env.VITE_HOST ?? 'localhost';
  const url = `ws://${apiHost}:${apiPort}/chat/${id}?username=${username}`;
  const socket: WebSocket = new WebSocket(url);

  const [room] = createResource(id, getRoom);
  const [events, setevents] = createSignal<RoomMessage[]>([]);
  const [users, setUsers] = createSignal<string[]>([]);

  onMount(async () => {
    socket.onmessage = (event) => {
      const eventData = JSON.parse(event.data);
      const newEvent: RoomMessage = { ...eventData, date: new Date() };
      setevents(events => [newEvent, ...events]);
      setUsers(() => newEvent.roomInfos.users);
    };
  });

  onCleanup(async () => {
    socket.close();
  });

  return (
    <main>
      <Show when={!room.loading} fallback={<span aria-busy="true">Loading ...</span>}>
        <h1>
          Room: {room()!.name}
        </h1>

        <div class='room'>

          <ul>
            <h2>Users</h2>
            <For each={users()}>
              {(user) => <li>{user}</li>}
            </For>
          </ul>

          <div class='chat'>
            <h2>Chat</h2>
            <div class='chat-messages'>
              <For each={events()}>
                {(event) => (
                  <div class='chat-message'>
                    <p>
                      {event.from} {event.date.toLocaleTimeString()}
                    </p>
                    <p>
                      {event.message}
                    </p>
                  </div>
                )}
              </For>
            </div>
            <RoomChat socket={socket} />
          </div>
        </div>

      </Show>
    </main>
  );
}