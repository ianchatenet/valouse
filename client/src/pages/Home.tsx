/* @refresh granular */

import { For, Setter, Show, createResource, createSignal } from 'solid-js';

import { A } from '@solidjs/router';
import { roomsPath } from '../routes';
import { RoomModel } from '../models/room';
import Modal from '../components/modal/Modal';
import { getRooms } from '../services/rooms/getRooms';
import { LocalStorageKeys } from '../enums/localStorageKeys.enum';
import { RoomCreateForm } from '../components/rooms/RoomCreateForm';

export default function Home() {
  const [rooms, setRooms] = createSignal<RoomModel[]>([]);
  const [data] = createResource(rooms, getRooms);
  const [userName, setuserName] = createSignal(localStorage.getItem(LocalStorageKeys.Username));

  let refModal: HTMLDialogElement;
  return (
    <main>
      <h1>Home page</h1>
      <Show when={userName()} fallback={<p>Set your user name.</p>}>
        <p>Welcome {userName()}.</p>
      </Show>

      <form onSubmit={(e) => handleOnSubmit(e, setuserName)}>
        <fieldset>
          <label>
            Enter your username
          </label>
          <input name='username' placeholder='your username' />
        </fieldset>
      </form>

      <button onClick={() => refModal.showModal()}> Create a Room</button>
      <Modal ref={refModal!}>
        <RoomCreateForm setRooms={setRooms} />
      </Modal>

      <Show when={!data.loading} fallback={<p>Searching...</p>}>
        <ul>
          <For each={data()}>
            {(room) => (
              <li>
                {room.name} <A href={roomsPath(room.id.toString())}>Join</A>
              </li>
            )}
          </For>
        </ul>
      </Show>
    </main>
  );
}

function handleOnSubmit(e: Event, setter: Setter<string | null>) {
  e.preventDefault();
  const form = e.target as HTMLFormElement;

  if (form !== null && form.elements) {
    const userName = (form.elements as any).username.value;
    localStorage.setItem(LocalStorageKeys.Username, userName);
    setter(userName);
  }
}