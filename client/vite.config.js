// vite.config.ts
import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';

export default defineConfig({
  plugins: [solidPlugin()],
  root: './client',
  build: {
    outDir: `${__dirname}/../public`
  },
  envDir: `${__dirname}/../`
});